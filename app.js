var promise = require('bluebird');
var path = require('path');
var fs = promise.promisifyAll(require('fs'));
var dir = path.resolve('../tmp/uploads');
var log = require('./logger').defaultLogger;
var CronJob = require('cron').CronJob;

var job = new CronJob({
    cronTime: '*/30 * * * *',
    onTick: function() {
    	fs.readdirAsync(dir)
    	.then(function (files) {
    		for(var i=0;i<files.length;i++){
    			(function(index){
    				fs.stat(dir+'/'+files[index], function (err, stats) {
    					if((Math.abs((new Date(stats.mtime)) - (new Date(Date.now()))) / 36e5) > 6){
    						console.log("Image deleted File Name = ",files[index]);
    						fs.unlink(dir+'/'+files[index]);
    					}
    				});
    			})(i);
    		}
    		return true;
    	});	
    },
    start: false,
    timeZone: 'Asia/Dhaka'
});

job.start();
